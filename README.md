# rbcd

Projeto básico Robocode. Vence 8 em 10 partidas contra o Crazy.

este projeto, mais do que simplesmente me despertar para os codegames novamente, me mostrou que um jogo extremamente simples pode ficar extremamente complexo, como é o caro dos robôs do nível do DrussGT, maior de todos os tempos. Se tivesse tido mais tempo, teria implementado movimento anti-gravitacional, enemy-traccking, e claro, o uso de métodos matemáticos do tipo K-means para melhorar o sistema de mira. Provavelmente ainda farei isso. Pois achei o jogo surpreendentemente divertido. Parabéns à IBM pela ideia. Parabéns à Solutis pela implementação desta prova. Obrigado. 

**Pontos fortes:**

Ganha do Crazy, que é totalmente aleatório, numa arena 800*800, no caso,o CryOS e mais 8 Crazies, no tipo Free for all.

**Ponto Fraco:**

Movimentação. Por enquanto, não pude implementar movimentação melhor, e por conta disso, se o CryOS nascer muito no canto, terá problemas.